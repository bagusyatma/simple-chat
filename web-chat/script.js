var name = prompt("what is your name?", "Crocodile");
const connection = new WebSocket("ws://localhost:8989");
var roomChat = document.getElementById("roomChat");

connection.onopen = function () {
  console.log("connect to server");
};

connection.onmessage = function (event) {
  const obj = new Object(event.data).toString();
  var json = JSON.parse(obj);
  
  if (json.name == name) {
    roomChat.innerHTML += `<li class="right clearfix">
                        <div class="chat-body clearfix">
                          <div class = "header"><strong class = "pull-right primary-font">` + json.name + `</strong><br></div>
                          <p class = "pull-right"> ` + json.message + ` </p>
                        </div>
                      </li>`
  } else {
    roomChat.innerHTML += `<li class="left clearfix">
                        <div class="chat-body clearfix">
                          <div class = "header"><strong class = "pull-left primary-font">` + json.name + `</strong><br></div>
                          <p class = "pull-left"> ` + json.message + ` </p>
                        </div>
                      </li>`
  }
};


document.getElementById("message").addEventListener("keyup", function (event) {
  if (event.keyCode === 13) {
    event.preventDefault();
    document.getElementById("myBtn").click();
  }
});

document.querySelector("button").onclick = function () {
  var message = document.getElementById("message").value;
  connection.send(
    JSON.stringify({
      name: name,
      message: message,
    })
    );
  };
